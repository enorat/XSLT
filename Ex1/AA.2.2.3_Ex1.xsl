<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:template match="/">
        <actividades>
            <xsl:for-each select="agenda/actividad">
                <xsl:if test="perfiles/perfil = 'Público general'">
                    <actividad>
                        <nombre><xsl:value-of select="nombre"/></nombre>
                        <xsl:copy select="perfiles">
                            <xsl:copy-of select="perfil"/>
                        </xsl:copy>
                        <fechainicio>
                            <xsl:value-of select="fechainicio"/>
                        </fechainicio>
                        <fechafin>
                            <xsl:value-of select="fechafin"/>
                        </fechafin>
                        <ficha>
                            <xsl:attribute name="url">
                                <xsl:value-of select="ficha"/>
                            </xsl:attribute>
                        </ficha>
                    </actividad>
                </xsl:if>
            </xsl:for-each>

        </actividades>
    </xsl:template>
</xsl:stylesheet>
