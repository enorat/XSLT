<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="/">
        <municipalities>
                <xsl:for-each select="municipis/poblacio">
                    <town>
                        <xsl:element name="name">
                            <xsl:attribute name="population">
                                <xsl:value-of select="nom/@habitants"/>
                            </xsl:attribute>
                            <xsl:attribute name="codi">
                                <xsl:value-of select="nom/@codi"/>
                            </xsl:attribute>
                            <xsl:value-of select="nom"/>
                        </xsl:element>
                        <surface>
                            <xsl:value-of select="superficie"/>
                        </surface>
                        <density>
                            <xsl:value-of select="densitat"/>
                        </density>
                        <updated>
                            <xsl:value-of select="actualitzat"/>
                        </updated>
                    </town>
                </xsl:for-each>
            
        </municipalities>
    </xsl:template>
</xsl:stylesheet>