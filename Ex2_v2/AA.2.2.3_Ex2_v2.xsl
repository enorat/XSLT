<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:template match="/">
        <municipis>
            <xsl:for-each select="municipis/poblacio">
                <xsl:sort select="municipis/poblacio/name/@habitants"/>
                <xsl:if test="nom/@habitants >= '39365'">
                    <xsl:element name="poblacio">
                        <xsl:attribute name="modificat">
                            <xsl:value-of select="modificat"/>
                        </xsl:attribute>
                        <xsl:element name="nom">
                            <xsl:value-of select="nom"/>                              
                        </xsl:element>
                        <xsl:element name="habitants">
                            <xsl:value-of select="nom/@habitants"/>
                        </xsl:element>
                        <xsl:copy-of select="superficie"></xsl:copy-of>
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
        </municipis>
    </xsl:template>
</xsl:stylesheet>
